<?php

namespace TechBBird;


use Cloudinary\Configuration\Configuration;
use Cloudinary\Cloudinary;

class CloudinaryClient
{

    /** @var Cloudinary */
    protected $cloudinary;

    public function __construct($cloudName, $apiKey, $apiSecret)
    {

        $config = new Configuration();
        $config->account->cloudName = $cloudName;
        $config->account->apiKey = $apiKey;
        $config->account->apiSecret = $apiSecret;
        $config->url->secure = true;
        $this->cloudinary = new Cloudinary($config);
    }

    /**
     * Expose the Cloudinary v2 Upload Functionality
     *
     */
    public function uploadApi()
    {
        return $this->cloudinary->uploadApi();
    }

    /**
     * Expose the Cloudinary v2 Upload Functionality
     *
     */
    public  function adminApi()
    {
        return $this->cloudinary->adminApi();
    }

    public  function searchApi()
    {

        return $this->cloudinary->searchApi();
    }
}
