<?php

namespace TechBBird;

use League\Flysystem\Config;
use League\Flysystem\FilesystemAdapter;
use TechBBird\CloudinaryClient;
use League\Flysystem\UnableToCopyFile;
use League\Flysystem\UnableToCreateDirectory;
use League\Flysystem\UnableToDeleteDirectory;
use League\Flysystem\UnableToDeleteFile;
use League\Flysystem\UnableToMoveFile;
use League\Flysystem\UnableToReadFile;
use League\Flysystem\UnableToRetrieveMetadata;
use League\Flysystem\UnableToSetVisibility;
use League\Flysystem\UnableToWriteFile;
use Cloudinary\Api\Exception\ApiError;
use League\Flysystem\FileAttributes;


/**
 * Class CloudinaryAdapter
 * @package TechBBird\CloudinaryAdapter
 */
class CloudinaryAdapter implements FilesystemAdapter
{
    /** @var CloudinaryClient */
    protected $cloudinaryClient;

    /** @var array */
    public $response;

    /**
     * Constructor
     * Sets configuration for Cloudinary Api.
     */
    public function __construct($cloudName, $apiKey, $apiSsecret)
    {
        $this->cloudinaryClient = new CloudinaryClient($cloudName, $apiKey, $apiSsecret);
    }

    public function fileExists(string $path): bool
    {
        return  $this->cloudinaryClient->uploadApi()->explicit($path) ? true : false;
    }
    public function write(string $path, string $contents, Config $options): void
    {

        $tempFile = tmpfile();
        if (@file_put_contents($tempFile, $contents, LOCK_EX) === false) {
            throw UnableToWriteFile::atLocation($path, error_get_last()['message'] ?? '');
        }

        $this->response = $this->writeStream($path, $tempFile, $options);
    }

    public function writeStream($path, $resource, Config $options): void
    {
        $publicId =  $options->get('public_id') ?? $path;

        $resourceType = $options->get('resource_type') ?? 'auto';

        $fileExtension = pathinfo($publicId, PATHINFO_EXTENSION);

        $newPublicId = $fileExtension ? substr($publicId, 0, - (strlen($fileExtension) + 1)) : $publicId;

        $uploadOptions = [
            'public_id'     => $newPublicId,
            'resource_type' => $resourceType
        ];

        $resourceMetadata = stream_get_meta_data($resource);

        $result = $this->cloudinaryClient->uploadApi()->upload($resourceMetadata['uri'], $uploadOptions);
        if (!$result) {
            $reason = error_get_last()['message'] ?? '';
            throw UnableToWriteFile::atLocation($path, $reason);
        }
        $this->response =  $result;
    }


    public function read($path): string
    {
        $resource = (array)$this->getResource($path);
        $file = file_get_contents($resource['secure_url']);
        if (!$file) {

            $reason = error_get_last()['message'] ?? '';
            throw UnableToReadFile::fromLocation($path, $reason);
        }
        return $file;
    }

    public function readStream($path)
    {
        $resource = (array)$this->cloudinary->adminApi()->resource($path);

        $stream = fopen($resource['secure_url'], 'rb');

        if (!$stream) {
            $reason = error_get_last()['message'] ?? '';
            throw UnableToReadFile::fromLocation($path, $reason);
        }
        return $stream;
    }


    public function delete($path): void
    {
        $response = $this->response = $this->cloudinaryClient->uploadApi()->destroy($path);
        if ($response['result'] !== 'ok') {
            throw UnableToDeleteFile::atLocation($path, '');
        }
    }

    public function deleteDirectory(string $path): void
    {
        try {

            $response = $this->cloudinaryClient->adminApi()->deleteResourcesByPrefix($path);
            if (empty($response['deleted'])) {
                throw UnableToDeleteDirectory::atLocation($path, " ");
            }
        } catch (ApiError $e) {
            throw UnableToDeleteDirectory::atLocation($path, $e->getMessage());
        }
    }

    public function createDirectory(string $path, Config $config): void
    {
        try {
            $response = $this->cloudinaryClient->adminApi()->createFolder($path);
            if (!$response['success']) {
                throw UnableToCreateDirectory::atLocation($path, '');
            }
        } catch (ApiError $e) {
            throw UnableToCreateDirectory::atLocation($path, $e->getMessage());
        }
    }


    public function setVisibility(string $path, string $visibility): void
    {
    }

    public function visibility(string $path): FileAttributes
    {
        return FileAttributes::fromArray($this->getMetadata($path));
    }

    public function mimeType(string $path): FileAttributes
    {
        return FileAttributes::fromArray($this->getMetadata($path));
    }

    public function lastModified(string $path): FileAttributes
    {
        return FileAttributes::fromArray($this->getMetadata($path));
    }


    function fileSize(string $path): FileAttributes
    {

        return FileAttributes::fromArray($this->getMetadata($path));
    }



    public function listContents(string $path, bool $deep): iterable
    {

        do {
            $response = (array)$this->cloudinaryClient->adminApi()->resources(
                [
                    'type' => 'upload',
                    'prefix' => $path,
                    'max_results' => 500,
                    'next_cursor' => isset($response['next_cursor']) ? $response['next_cursor'] : null,
                ]
            );
            yield FileAttributes::fromArray($this->prepareResourceMetadata($response['resources']));
        } while (array_key_exists('next_cursor', $response));
    }

    public function move(string $source, string $destination, Config $config): void
    {
        $pathInfo    = pathinfo($source);
        $newPathInfo = pathinfo($destination);

        $remotePath = ($pathInfo['dirname'] != '.') ? $pathInfo['dirname'] . '/' . $pathInfo['filename'] : $pathInfo['filename'];

        $remoteNewPath = ($pathInfo['dirname'] != '.') ? $newPathInfo['dirname'] . '/' . $newPathInfo['filename'] : $newPathInfo['filename'];

        $result = $this->cloudinaryClient->uploadApi()->rename($remotePath, $remoteNewPath);
        if (!$result) {
            throw UnableToMoveFile::fromLocationTo($source, $destination);
        }

        $result['public_id'] == $newPathInfo['filename'];
        $this->response = $result;
    }


    public function copy(string $source, string $destination, Config $config): void
    {
        $response = $this->cloudinaryClient->uploadApi()->upload($source, ['public_id' => $destination]);
        if (!$response) throw UnableToCopyFile::fromLocationTo($source, $destination);
        $this->response = $response;
    }



    /**
     * Prepare apropriate metadata for resource metadata given from cloudinary.
     * @param array $resource
     * @return array
     */
    protected function prepareResourceMetadata($resource)
    {
        $resource['type'] = 'file';
        $resource['path'] = $resource['public_id'];
        $resource['file_size']  =  $this->prepareSize($resource)['size'];
        $resource['last_modified'] = $this->prepareLastModified($resource) ??  $this->prepareTimestamp($resource)['timestamp'];
        $resource['mime_type'] = $this->prepareMimetype($resource)['mimetype'];
        return $resource;
    }

    /**
     * prepare size response
     *
     * @param array $resource
     *
     * @return array
     */
    protected function prepareSize($resource)
    {
        $size = $resource['bytes'];
        return compact('size');
    }

    /**
     * prepare timestamp response
     *
     * @param array $resource
     *
     * @return array
     */
    protected function prepareTimestamp($resource)
    {
        $timestamp = strtotime($resource['created_at']);
        return compact('timestamp');
    }

    protected function prepareLastModified($resource)
    {
        return strtotime(array_values(array_filter(
            $resource['version'],
            function ($version) use ($resource) {
                return $version === $resource['versions']['version'];
            }
        ))[0]['time']);
    }

    /**
     * prepare mimetype response
     *
     * @param array $resource
     *
     * @return array
     */
    protected function prepareMimetype($resource)
    {
        $mimetype = $resource['resource_type'];
        return compact('mimetype');
    }

    /**
     * Get all the meta data of a file or directory.
     *
     * @param string $path
     *
     * @return array|false
     */
    public function getMetadata($path)
    {
        if ($metaData = $this->prepareResourceMetadata($this->getResource($path))) {
            return $metaData;
        }
        throw UnableToRetrieveMetadata::fileSize($path);
    }

    /**
     * Get Resource data
     * @param string $path
     * @return array
     */
    public function getResource($path)
    {
        return (array)$this->cloudinaryClient->adminApi()->resource($path);
    }

    /**
     * Get all the meta data of a file or directory.
     *
     * @param string $path
     *
     * @return array|false
     */
    public function getSize($path)
    {
        return $this->prepareSize($this->getResource($path));
    }

    /**
     * Get the mimetype of a file.
     *
     * @param string $path
     *
     * @return array|false
     */
    public function getMimetype($path)
    {
        return $this->prepareMimetype($this->getResource($path));
    }

    /**
     * Get the timestamp of a file.
     *
     * @param string $path
     *
     * @return array|false
     */
    public function getTimestamp($path)
    {
        return $this->prepareTimestamp($this->getResource($path));
    }
}
